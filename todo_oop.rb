# $todo = ["Homework", "Study", "Project", "College Work"];
# # $todos = $todo.map {|i| i.downcase}
# $todo.map!(&:downcase)

class Todo
  def initialize
    @todo = Array.new
  end

  def choices
    choice ="y"
  while choice =="y" or choice=="Y"
    puts "TODO APPLICATION\n\n"
    puts "1. List all todos\n\n2. Add new item in todos\n\n3. Remove item from todos\n\n4. Update item in todos\n\n5. Exit\n\n"
    print "Enter Your Selection :"
    choose = gets.chomp.to_i ; print("\n")
      
    case(choose)  
      when 1
        list_array
      when 2
        add_in_array
      when 3
        remove_from_array
      when 4
        update_array
      when 5
        exit
      else
        puts("Invalid Selection")
      end
      print "Continue (Y/y)? : "
      choice = gets.chomp
      print("\n")
  end
end

  def list_array
  @todo.each do |todo|
    puts todo
    print ("\n")
  end
end

def add_in_array
  puts("Enter list to be added")
  todo_add = gets.chomp
  @todo.push(todo_add.downcase)
  puts(" Added")
end

def remove_from_array
  puts("What do you want to remove from array")
  todo_remove = gets.chomp
  @todo.delete(todo_remove.downcase)
  puts("\n Removed")
end

def update_array
  puts("What do you want to update in array")
  todo_update = gets.chomp
  todo_update_case = todo_update.downcase
  if(@todo.include?(todo_update_case))
    x = @todo.index(todo_update_case)
    print("Enter the new list now : ")
    new_update = gets.chomp
    @todo[x]=new_update.downcase
    print("UPDATED\n")
  else
    puts("The input is not in the list")
  end
end
end

t1 = Todo.new  
t1.choices

