d = {one:1, two:2, three:3}

d.each {|key, value| puts "#{key} is #{value}"}

d.each_key {|key| puts " Key is \"#{key}\""}

d.each_value {|value| puts "Value is \"#{value}\""}

print d.keys
print d.values
print ("\n")

puts d.key?(:one)
puts d.value?(1)

puts d.fetch("one", "Not found")

#select
puts "Select"
hash_select = d.select do |key, value|
  value.even?
end
puts hash_select

#slice
puts "Slice"
hash_slice = d.slice(:one, :two)
puts hash_slice

#reject
puts = "Reject"
puts d.reject {|x,y| y.even?}