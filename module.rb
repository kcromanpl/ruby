module HELLO
  def speak(sound)
    puts sound
  end
end

class Dog
  include HELLO
end

class Human
  include HELLO
end

beli = Dog.new
beli.speak("Hau")

Roman = Human.new
Roman.speak("HELLO")