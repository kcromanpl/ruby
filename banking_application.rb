# require 'pry'

class Banking
  @@account_list = Hash.new
  @@balance_list = Hash.new

  def initialize(acc_no,pin,balance = 100)
    @@account_list.store(acc_no,pin)
    @@balance_list.store(acc_no,balance)
  end

  def continue
    print "Continue (Y/y)? or Check Account list (A/a) : "
    choice = gets.chomp.downcase
    if (choice == "y")
      access
    elsif (choice =='a')
      list_accounts
        sleep(2) #program waits for 2 seconds
      access
    else
      puts "TERMINATED"
      exit
    end
  end
  
  def list_accounts
    puts"Account : #{@@account_list.keys} & Pin:#{@@account_list.values} & Balance :#{@@balance_list.values}  \n"
  end
  
  def account_check
    puts "WELCOME !!! RUBY CLI BANKING SYSTEM !!!"
    print("Enter Your Account number: ")
    @account = gets.chomp.to_i;
    if (@@account_list.has_key?(@account))
      pin_check
    else
      puts "Account Not Found"
    end
  end

  def pin_check
    print ("Enter your PIN: ")
        pin_number = gets.chomp.to_i
        if (@@account_list[@account] == pin_number)
          print ("\nPIN Matched\n")
          access
        else
          print ("\nPIN : No Match\n")
        end
  end

  def access
    puts "\nq. Query the balance\n\nd. Deposit Money\n\nw. Withdraw money\n\n"
    print ("Enter Your Selection (d\q\w): ")
    action = gets.chomp.downcase
      case action
        when "d"
          deposit
        when "q"
          query
        when "w"
          withdraw
        else
          puts "Invalid Selection"
      end
  end

  def deposit
    print "Enter the amount you want to deposit: "
    @amount = gets.chomp.to_i;
    if (@amount.between?(100, 10000))
      @@balance_list[@account] += @amount
      # binding-pry
      puts " Balance Deposited : The updated balance is #{@@balance_list[@account]}\n\n"
      continue
    else
      puts"Value too low or high, Re-enter the value"
      deposit
    end
  end

  def query
    puts "Your current balance is #{@@balance_list[@account]}"
    continue
  end

  def withdraw
    puts "Your current balance is #{@@balance_list[@account]}"

    print("Enter the amount to withdraw: ")
    @deduct = gets.chomp.to_i
    if (@@balance_list[@account] > @deduct && @@balance_list[@account].between?(100,10000) && @deduct.between?(100,10000) )
      
      @@balance_list[@account] -= @deduct
      puts "Balance Deducted : Your current balance is #{@@balance_list[@account]}"
      continue
    else
      puts "ERROR : Value too low or high, Re-enter the value"
      withdraw
    end
  end

end #end of class

#only allowing admin class to access list_accounts

=begin 
class Admin < Banking
  def list_accounts
    puts"Account : #{@@account_list.keys} & Pin:#{@@account_list.values} & Balance :#{@@balance_list.values}  \n"
  end
end

Admin.new(01,01,5000).list_accounts
=end

Banking.new(56,0,5000).account_check