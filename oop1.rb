class Person
  @@person_count= 0

  def initialize(name,address)
    @name = name
    @address = address
    @@person_count+=1
  end

  def self.check
    @@person_count
  end

  def saysHI #getter
    puts "Hi #{@name} from #{@address}"
  end

  def put_name_address(name, address) #setter
    @name = name
    @address = address
  end

end

# Person.person_count  #classname le access garna milena

# puts Person.class_variable_get(:@@person_count);

puts Person.check #to access Class variable ; NO OBJECT NEEDED

p1= Person.new("Roman", "Lalitpur")
p1.saysHI
puts Person.check

print "Enter name "; name = gets.chomp;
print "Enter address "; address = gets.chomp;
p1.put_name_address(name,*address)
p1.saysHI

p1= Person.new("Romann", "Lalitpurr")
puts Person.check