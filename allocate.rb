class Box
  attr_accessor :width, :height

  def initialize(w,h)
     @width, @height = 0, 0
  end

  def setArea(w,h)
    @width, @height = w, h
  end

  # instance method
  def getArea
     @width * @height
  end
  
end

box1 = Box.new(10, 20)

# call instance method using box1
a = box1.getArea()
puts "Area of the box is : #{a}"

# create another object using allocate
box2 = Box.allocate

# call instance method using box2
box2.width = 10
box2.height = 20
a = box2.getArea()
puts "Area of the box is : #{a}"
puts box2.inspect