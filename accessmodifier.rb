class Rect
  def initialize(w,h)
    @width, @height = w,h
  end

  def getWidth
    @width
  end

  def getHeight
    @height
  end

  private :getWidth,:getHeight

  def getArea
    getWidth() * getHeight
  end

  def printArea
    @area = getWidth() * getHeight
    puts "#{@area}"
  end

  # protected :printArea
end

ct = Rect.new(10,20)
=begin
 accesing private methods

puts ct.getWidth
puts ct.getHeight

=end

puts ct.getArea
ct.printArea

 
