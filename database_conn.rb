require 'mysql2'

begin
  con = Mysql2::Client.new(:host => "localhost", :username => "Roman", :password => "Roman123", :database => "Students")
  result_set = con.query('SELECT * FROM student_info')
  result_set.each do |row|
    puts"#{row}"
  end
  
rescue Mysql2::Error => e
  puts "ERROR #{e.message}"
  exit 1
ensure
  con.close if con
end
