array = ["blah",:meow,"a2",3,:building]
b = []

#print if symbol
array.each do |i|
  if i.instance_of?(Symbol)
    b.push(i)
  end
end
p array
p b

#print array with index

array.each_with_index do |value,index|
  p "#{index}\. #{value}"
end

#adding prefixes in values

last_names = ["D", "Krugman"]
last_names.map! do |name|
  "Paul" + " " + name
end
puts last_names
p "finish"

#containing a

last_names.select! do |name|
  name.include?("m")
end
puts last_names

#sentence making from array

people = [["Lebron", "cool dude"], ["Bieber", "jerk face"]]
puts "#{people[0][0]} is a #{people[0][1]} #{people[1][0]} is a #{people[1][1]}"

#array value compare

test_scores = [["jon", 99,0], ["sally", 65,90], ["bill", 85,30]]
test_scores.select! do |name, score, value|
 value > 80
end
print test_scores
puts ("FInish")

#getting first from comparing

ages = [[:frank, 42], [:sue, 77], [:granny, 77]]
ages.select! do |name, value|
  puts name
  puts value
  
  # value == 77
end
print ages
