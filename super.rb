require 'pry'
class Person
  def initialize(name)
      @name = name
  end
  def name
    puts "#{@name} from SUPER CLASS"
  end
  def test
    puts "TEST from SUPER CLASS"
  end
end

class Student < Person
  def initialize (name,roll)
    super("Roman")
    @roll = roll
  end
  def name
    puts"#{@name} FROM SUB CLASS"
    puts"#{@roll} FROM SUB CLASS"
  end
  def test
    print("\n\nTEST\n\n")
    super
    puts "Now from Sub class"
  end
end

p = Student.new("Roman",26)
p.name
p.test

# binding-pry
