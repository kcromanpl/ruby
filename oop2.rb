class Person
  # attr_reader :name, :address
  # attr_writer :name
  attr_accessor :name, :address
  def initialize(name, address)
    @name = name
    @address = address
  end
end
p1 = Person.new("Roman", "lubhoo")

p1.name = "Hello"
puts p1.name
puts p1.address