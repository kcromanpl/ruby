class Person
  attr_accessor :name, :address

  def initialize(name,address)
    @name = name
    @address = address
  end

  def setnameadd(name,address)
    @name = name
    @address = address
  end

  def printPerson
    puts "Basically prints - Class Person"
  end

end

class Student < Person
  attr_accessor :college, :roll_no

  def initialize(college, roll_no)
    @college = college
    @roll_no = roll_no
  end

  def access(name)
    @name = name
  end
end


s1 = Student.new("Patan",26)
s1.setnameadd("Roman", "Lubhoo")
puts s1.name, s1.address, s1.college, s1.roll_no

s1.printPerson
