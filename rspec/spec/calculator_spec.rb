require '../../calculator.rb'

describe "Calculator" do
  it "does normal calculation" do
    calc = Calculator.new
  end
end

describe "add" do
  it "adds two numbers" do
    calc = Calculator.new
    expect(calc.add(2,3)).to eql(5)
  end
end

describe "sub" do
  it "Subtracts two numbers" do
    calc  = Calculator.new
    expect(calc.sub(2,3)).to eq(-1)
  end
end

describe "mul" do
  it"Multiplies two numbers" do
    calc = Calculator.new
    expect(calc.mul(2,3)).to eq(6)
  end
  end

describe "div" do
  it"Divides two numbers num1 / num2" do
  calc = Calculator.new
  expect(calc.div(6,3)).to eq(2)
end
end

describe "sqrt" do
  it"Find Square root of two number" do
    calc = Calculator.new
    expect(calc.sqrt(4)).to eql(2.0)
  end
end


