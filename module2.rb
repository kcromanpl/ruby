require './module.rb'

module Swim
  def swi
    puts "Swim from Swim module"
  end
end

class Animal
  include HELLO
end

class Fish < Animal
  include Swim
  def swi
    puts"Swim from Fish class"
  end
end

a1 = Animal.new
a1.speak("YO")

f1 = Fish.new
f1.swi