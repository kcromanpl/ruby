=begin 
def if_vote_check(age)
  if age < 18
    puts "Not eligible"
  elsif age == 18
    puts "OK"
  else
    puts "Eligible" 
  end
end
if_vote_check(20)
if_vote_check(15)

def switch_vote_check(age)
  case(age)
    when 0..17
      puts "Not Eligible"
    when 18
      puts "OK"
    when 19..100
      puts "Eligible"
    else
      puts "Invalid"
  end
end
switch_vote_check(20)
switch_vote_check(18)

# print "x is 1\n" if x == 1
x = gets.chomp.to_i

unless x == 1
  puts "X"
else
  puts "Y"
end

=end


