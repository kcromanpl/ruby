#Add an instance method to the Insect class to calculate the age_in_years.
=begin
class Insect
  def initialize(age_in_days)
    @age_in_days = age_in_days
  end
  def age_in_years
    @age_in_days.to_f / 365
end

#Create a Lamp class with a class method called about_me that returns the String "We brighten up people's lives".
class Lamp
  def self.about_me #(OR Lamp.about_me)
    puts ("We brighten up people's lives")
  end
end
Lamp.about_me
=end
#Create a module called MathHelpers with the exponent() method that takes a two numbers as arguments and raises 
# the first number to the power of the second number. Create a class called Calculator with a method called square_root() 
# that takes the square root of the number (raises it to the power of 0.5). The square_root() method should use 
# the exponent() method.
module MathHelpers
  def exponent(number, power)
    Math.pow(number,power)
end
end
class Calculator
  def sqrt(number)
    include MathHelpers
    exponent(number,0.5)
  end
end
c = Calculator.new
puts  
