#String Concatenation

name = gets.chomp

# puts("Hello" + " " + name)

# puts("Hello #{name}")

puts ("Hello %s" %name)
puts("Hello #{name.upcase}")

puts ("length of the string is #{name.length}")
puts ("Splitting #{name.split}")
puts ("Replacing Your String with Hi if it contains Hello : #{name.sub("Hello","Hi")}")
print("If the name contains 'a' : ")
puts name.include?('a')

